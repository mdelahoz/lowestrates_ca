# TASK

Please create the template design attached using Plain HTML, CSS and JS.
NO FRAMEWORKS like Bootstrap or JQuery please.
(Responsive, has mobile and desktop versions.)

The design should be mobile first. The css and html should be clean and simple.
Clicking on the hamburger bars should open the menu on the left side.

## JS requirement:
When clicking the login button on the top right hand side, please create a JavaScript to:
1. Jump to the bottom of the page.
2. Open the menu on the side.
3. When the menu is opened, add the following menu items to the top of the list of
existing items: &quot;Loans&quot;, &quot;Finance&quot;, and &quot;Debt&quot;.
4. When the user closes the menu, please make the JS to reset to the
original list and jump back to the top of the page.
5. Please make sure the JS function only happens when the user clicks on
login. If the user simply clicks on the menu on the TOP LEFT hand corner,
the JS feature SHOULD NOT BE executed.

#Solution and Assumptions
1. Based on the requirements pointed in the screenshots the plain html structure is:
 
- header.
- navigation
- main content
- footer

2. the folder structure is

- root
    - assets. contains images
    - css. contains the main stylesheet for the html.
    - scripts. contains the JS to meet the JS requirement.
    
    
 I focus on the Task, so I used place holder for the pagination and hardcoded values for the navigation and categories.
 
 the css is commented. The css for the header is at the top next is the css for navigation. Next the css for main content along with the media queries and finally the css for the footer ant the end of the file
    
   
