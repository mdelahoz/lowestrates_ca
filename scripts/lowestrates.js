function openNav() {
  document.getElementById("menuOnTheSide").style.width = "250px";
}

function closeNav() {
  // 4. When the user closes the menu, please make the JS to reset to the
  // original list and jump back to the top of the page.
  const menu = document.getElementById("menuOnTheSide");

  const menuItems = menu.children;

  let itemsToRemove = [];

  for (let menuItemsKey in menuItems) {

    if (menuItems[menuItemsKey].hasAttribute("class")) {
      menu.style.width = "0";
      break;
    } else {
      itemsToRemove.push(menuItems[menuItemsKey]);
    }
  }
  if (itemsToRemove.length > 0) itemsToRemove.forEach(el => el.remove());
}

function addMenuItems() {
  const items = ["Debt", "Finance", "Loans"];
  const newItems = items.map(item => {
    const newItem = document.createElement('a');
    const text = document.createTextNode(item);
    newItem.appendChild(text);
    newItem.href = "#";
    return newItem;
  });

  newItems.forEach(newItem => {
    const menuItem = document.getElementById("menuOnTheSide");
    menuItem.insertBefore(newItem, menuItem.childNodes[0]);
  });
}

function login() {
  // 1. Jump to the bottom of the page.
  scrollTo(0, document.body.scrollHeight);

  // 2. Open the menu on the side.
  openNav();

  // 3. When the menu is opened, add the following menu items to the top of the list of
  // existing items: &quot;Loans&quot;, &quot;Finance&quot;, and &quot;Debt&quot;.
  addMenuItems();

  // 5. Please make sure the JS function only happens when the user clicks on
  // login. If the user simply clicks on the menu on the TOP LEFT hand corner,
  //   the JS feature SHOULD NOT BE executed.
}
